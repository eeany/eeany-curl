package http

import "gitee.com/eeany/eeany-curl/fastHttp"

// New 初始化 http模块
func New(config Config) IHttp {
	switch config.ConfigType {
	case "fastHttp":
		return fastHttp.New()
	}
	return nil
}

type Config struct {
	ConfigType string
}
