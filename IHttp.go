package http

//IHttp http工具
type IHttp interface {
	Get(url string) ([]byte, error)
	PostJsonData(url string, param map[string]interface{}) ([]byte, error)
	GetString(url string) string
	PostJsonDataString(url string, param map[string]interface{}) string
	PostStringDataString(url string, param string) string
	PostStringDataByte(url string, param string) []byte
}
