package fastHttp

import (
	"fmt"
	"testing"
)

func TestFastHttp_PostJsonData(t *testing.T) {
	type fields struct {
		timeout int
	}
	type args struct {
		url   string
		param map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{

		{
			name: "1",
			args: args{
				url: "http://10.18.217.251:8080/rs/1.0/jhk",
				param: map[string]interface{}{
					"bid":     "100001",
					"user_id": "8610030090000010000006055f2718b7",
					"uid": map[string]string{
						"uid1": "112111112",
					},
					"plat_type":    0,
					"plat_version": "4.2",
					"bar_id":       "202",
					"bar_type":     "201",
					"license":      "1015",
					"device_type":  "0",
					"model_id":     "86100300900000100000060f:01.102.043",
					"adpos":        "280",
					"item_id":      "282032",
					"page_id":      1,
				},
			},
		}, {
			name: "1",
			args: args{
				url: "http://10.18.217.251:8080/rs/1.0/jhk",
				param: map[string]interface{}{
					"bid":     "100001",
					"user_id": "8610030090000010000006055f2718b7",
					"uid": map[string]string{
						"uid1": "112111112",
					},
					"plat_type":    0,
					"plat_version": "4.2",
					"bar_id":       "202",
					"bar_type":     "201",
					"license":      "1015",
					"device_type":  "0",
					"model_id":     "86100300900000100000060f:01.102.043",
					"adpos":        "280",
					"item_id":      "282032",
					"page_id":      1,
				},
			},
		},
	}
	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {
			fastHttp := FastHttp{}
			got, err := fastHttp.PostJsonData(tt.args.url, tt.args.param)

			fmt.Println(string(got))
			fmt.Println(err)
		})
	}
}

func TestFastHttp_Get(t *testing.T) {
	type fields struct {
		timeout int
	}
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "11111",
			args: args{
				url: "http://10.18.217.251:8080/ds/1.0/jhk?bid=31&adpos=121&plat_version=1.0&plat_type=1&actionType=GET&prefixKey=121&suffixKey=I1&sep=,&key=46431:2001",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fastHttp := FastHttp{}
			got, err := fastHttp.Get(tt.args.url)

			fmt.Println(string(got))
			fmt.Println(err)
		})
	}
}

func TestFastHttp_PostStringDataString(t *testing.T) {
	type args struct {
		url   string
		param string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "test___1",
			args: args{
				param: `{
	"size": 10,
	"query": {
		"bool": {
			"should": [
				{
					"bool": {
						"must": [
							{
								"term": {
									"is_fixed": true
								}
							},
							{
								"range": {
									"op_column_index": {
										"lte": 100
									}
								}
							}
						]
					}
				}
			],
			"filter": [
				{
					"term": {
						"feature_code": "86100300900000800000060c"
					}
				},
				{
					"term": {
						"sys_version_code": "tvtest"
					}
				},
				{
					"term": {
						"license_code": "1007"
					}
				},
				{
					"term": {
						"chan_id": "1027"
					}
				}
			]
		}
	},
	"sort": [
		{
			"_id": {
				"unmapped_type": "keyword",
				"order": "desc"
			}
		}
	],
	"from": 0
}`,
				url: "http://10.18.220.157:9200/v1_90_channel_layout/_search",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fastHttp := FastHttp{}
			got := fastHttp.PostStringDataString(tt.args.url, tt.args.param)
			fmt.Println(got)
		})
	}
}
